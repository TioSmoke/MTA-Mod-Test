resourceRoot = getResourceRootElement(getThisResource())
localPlayer = getLocalPlayer()
sx, sy = guiGetScreenSize(), getThisResource()

addEventHandler("onClientResourceStart", resourceRoot, function()
  fadeCamera(true)
  setCameraMatrix(979.67584,-675.50397,121.97626,981.08551,-670.62762,121.97626)
  startInfoWindow()
end
)

addEvent("updateTestPedClothes", true)
updateTestPedClothes = function(cloth0, cloth2, cloth3, cloth15, cloth16)
	if isElement ( testPed ) then
		setPedClothes (testPed,0,cloth0)
		setPedClothes (testPed,2,cloth2)
		setPedClothes (testPed,3,cloth3)
		setPedClothes (testPed,15,cloth15)
		setPedClothes (testPed,16,cloth16)
	end
end
addEventHandler("updateTestPedClothes", getRootElement(), updateTestPedClothes)

function setPedClothes(thePed, clothingSlot, clothingID)
	if not isElement(thePed) or type(clothingSlot) ~= "number" then
		error("Invalid arguments to setPedClothes()!", 2)
	end
 
	if not clothingID or clothingID == -1 then
		return removePedClothes(thePed, clothingSlot)
	end
 
	local hasClothes = getPedClothes(thePed, clothingSlot) 
	if hasClothes then
		removePedClothes(thePed, clothingSlot)
	end
	
	local texture, model = getClothesByTypeIndex(clothingSlot, clothingID)
	return addPedClothes(thePed, texture, model, clothingSlot)
end


local screenWidth,screenHeight = guiGetScreenSize()

function startInfoWindow ()
	exports.ibnew:createImageButton ( 'play_button', screenWidth/2-456, screenHeight/2, 256, 40, "Jogar")
	exports.ibnew:createImageButton ( 'player_button', screenWidth/2-456, screenHeight/2+45, 256, 40, "Roupas" )
	exports.ibnew:createImageButton ( 'quit_button', screenWidth/2-456, screenHeight/2+90, 256, 40, "Sair" )
	addEvent ( "imageButtonClicked", true )
	addEventHandler ( 'imageButtonClicked', root, showWeaponSelect)
	showCursor ( true )
	setPlayerHudComponentVisible ( 'radar', false )
	light = createEffect ( "smoke_flare", 1988, -765, 131)
end

GUIEditor = {
    label = {},
    staticimage = {}
}
function showWeaponSelect (button)
	if button.name == 'play_button' then
		if isTransferBoxActive() then
			if GUIEditor.staticimage[1] then return end
			GUIEditor.staticimage[1] = guiCreateStaticImage(0.42, 0.37, 0.13, 0.12, ":Design/images/white.png", true)
			guiSetProperty(GUIEditor.staticimage[1], "ImageColours", "tl:FF1A1919 tr:FF1A1919 bl:FF1A1919 br:FF1A1919")

			GUIEditor.staticimage[2] = guiCreateStaticImage(0.27, 0.70, 0.46, 0.30, ":Design/images/white.png", true, GUIEditor.staticimage[1])
			guiSetProperty(GUIEditor.staticimage[2], "ImageColours", "tl:FF6B0101 tr:FF6B0101 bl:FF6B0101 br:FF6B0101")

			GUIEditor.label[1] = guiCreateLabel(0.00, 0.00, 1.00, 1.00, "Fechar", true, GUIEditor.staticimage[2])
			guiSetFont(GUIEditor.label[1], "default-bold-small")
			guiLabelSetHorizontalAlign(GUIEditor.label[1], "center", false)
			guiLabelSetVerticalAlign(GUIEditor.label[1], "center")
			addEventHandler("onClientGUIClick", GUIEditor.label[1], function()
				if GUIEditor.staticimage[1] then
					destroyElement(GUIEditor.staticimage[1])
					GUIEditor.staticimage[1] = false
				end
			end)
			GUIEditor.label[2] = guiCreateLabel(0.11, 0.02, 0.78, 0.57, "Você deve esperar o download acabar", true, GUIEditor.staticimage[1])
			guiLabelSetHorizontalAlign(GUIEditor.label[2], "center", true)
			guiLabelSetVerticalAlign(GUIEditor.label[2], "center")
			setTimer(function()
				if GUIEditor.staticimage[1] then
					destroyElement(GUIEditor.staticimage[1])
					GUIEditor.staticimage[1] = false
				end
			end, 5000, 1)
			return
		end
		showCursor ( false )
		triggerServerEvent ("loginPlayerBySeral",localPlayer)
		exports.ibnew:destroyImageButtonByName("play_button")
		exports.ibnew:destroyImageButtonByName("player_button")
		exports.ibnew:destroyImageButtonByName("quit_button")
		removeEventHandler ( 'imageButtonClicked', root, showWeaponSelect)
		if isElement(testPed) then
		destroyElement ( testPed ) 
		end
		if isElement(light) then
		destroyElement ( light ) 
		end
	elseif button.name == 'player_button' then
		showCursor ( false )
		if isElement(background_front) then
		guiSetVisible(background_front, false)
		end
		exports.cloth:showClothWindow(testPed)
		exports.ibnew:destroyImageButtonByName("play_button")
		exports.ibnew:destroyImageButtonByName("player_button")
		exports.ibnew:destroyImageButtonByName("quit_button")
		removeEventHandler ( 'imageButtonClicked', root, showWeaponSelect)
	elseif button.name == 'quit_button' then
		triggerServerEvent ("playerWantToQuit",localPlayer)
	end
end

showLoginWindow = function(bool)
	setElementData(getLocalPlayer(), "clickedButton", false)
	showCursor(bool)
	if bool then
		guiSetPosition(background_front, 0.2, -0.75, true)
		addEventHandler("onClientRender", getRootElement(), rollLoginPanel)
		rollProgress = 1
		rollIn = true
		guiSetInputMode("no_binds_when_editing")
	else
		guiSetPosition(background_front, 0.2, 0.25, true)
		addEventHandler("onClientRender", getRootElement(), rollLoginPanel)
		rollProgress = 0
		rollIn = false
		guiSetInputMode("allow_binds")
	end
	randomDirAnim = math.random() > 0.5 and -1 or 1
	--if math.random() > 0.5 then
		useXAxis = true
		animType = useXAxis and "InBounce" or "InElastic"
	--end
end

rollLoginPanel = function()
	local eval = nil
	if rollIn then
		if rollProgress > 0 then
			rollProgress = (rollProgress * 1000 - 15) / 1000
			if rollProgress < 0 then
				rollProgress = 0
			end
			eval = getEasingValue(rollProgress, animType)
		else
			removeEventHandler("onClientRender", getRootElement(), rollLoginPanel)
			return true
		end
	else
		if rollProgress < 1 then
			rollProgress = (rollProgress * 100 + 3) / 100
			if rollProgress > 1 then
				rollProgress = 1
			end
			eval = getEasingValue(rollProgress, "InQuad")
		else
			removeEventHandler("onClientRender", getRootElement(), rollLoginPanel)
			return true
		end
	end
	if useXAxis then
		guiSetPosition(background_front, 0.2, 0.25 + randomDirAnim * eval, true)
	else
		guiSetPosition(background_front, 0.2 + randomDirAnim * eval, 0.25, true)
	end
	guiSetVisible(background_front, true)
end


